package PACMAN;

import java.io.Serializable;

public abstract class Caracters implements Serializable{
	
	int x;
	int y;
	
	public Caracters(int x, int y) {
		this.x = x;
		this.y = y;
	}
/**
 * Función con la que se mueven los personajes por la matriz
 */
	public abstract void moverse();
}

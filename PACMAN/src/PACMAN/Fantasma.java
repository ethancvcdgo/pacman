package PACMAN;

import java.io.Serializable;
import java.util.Random;

public class Fantasma extends Caracters implements Serializable {

	/**
	 * Ultima dirección en la que se han movido los fantasmas
	 */
	int ultimaDir;
	/**
	 * Tiemo que le queda al fantasma para reaparecer
	 */
	int cooldown;
	/**
	 * num del fantasma en la matriz
	 */
	int f;
	/**
	 * Si el fantasma esta muerto o no
	 */
	boolean muerto;
	/**
	 * Para hacer que el fantasma se mueva mas lento(acumulador)
	 */
	int movfan;

	/**
	 * Consructor de los fantasmas
	 * 
	 * @param x
	 * @param y
	 * @param f num del fantasma en la matriz
	 */
	public Fantasma(int x, int y, int f) {
		super(x, y);
		this.f = f;
		this.ultimaDir = 0;
		this.cooldown = 0;
		this.muerto = false;
		this.movfan = 0;
	}

	@Override
	public void moverse() {
		// cada fantasma se mueve diferente, uno random, uno te persigue a saco, otro va
		// a donde estaras proximamente.(1 arriba, 2 derecha, 3 abajo, 4 izquierda)
		if (cooldown == 0 && !muerto) {
			Random r = new Random();
			int Dirf = r.nextInt(4) + 1;
			boolean nomuro = false;
			while (!nomuro) {
				if (Dirf == 1 && (Mapa.fanta[x][y - 1] == 9 || ultimaDir == 3)) {
					Dirf = r.nextInt(4) + 1;
				} else {
					nomuro = true;
				}
				if (Dirf == 2 && (Mapa.fanta[x + 1][y] == 9 || ultimaDir == 4)) {
					Dirf = r.nextInt(4) + 1;
				} else {
					nomuro = true;
				}
				if (Dirf == 3 && (Mapa.fanta[x][y + 1] == 9 || ultimaDir == 1)) {
					Dirf = r.nextInt(4) + 1;
				} else {
					nomuro = true;
				}
				if (Dirf == 4 && (Mapa.fanta[x - 1][y] == 9 || ultimaDir == 2)) {
					Dirf = r.nextInt(4) + 1;
				} else {
					nomuro = true;
				}
			}
			if (f == 2 && PACMAN.droja == false) { // cosa que funciona a medias
				int pacx = 0;
				int pacy = 0;
				for (int i = 0; i < Mapa.campo.length; i++) {
					for (int j = 0; j < Mapa.campo[0].length; j++) {
						if (Mapa.campo[i][j] == 1) {
							pacx = i;
							pacy = j;
						}
					}
				}
				int distx = pacx - x;
				int disty = pacy - y;
				Mapa.fanta[x][y] = 0;

				if (Math.abs(distx) > Math.abs(disty)) {
					if (distx > 0) {
						if (Mapa.fanta[x + 1][y] != 9 && Mapa.fanta[x + 1][y] != 8 && ultimaDir != 4) {
							x++;
							ultimaDir = 2;
						} else {
							if (Dirf == 1 && ultimaDir != 3) {
								if (Mapa.fanta[x][y - 1] != 9 && Mapa.fanta[x][y - 1] != 7) {
									Mapa.fanta[x][y] = 0;
									y--;
									Mapa.fanta[x][y] = f;
									ultimaDir = Dirf;
								} else if (Mapa.fanta[x][y - 1] == 7) {
									Mapa.fanta[x][y] = 0;
									y = Mapa.fanta.length - 1;
									Mapa.fanta[x][y] = f;

								}
							} else if (Dirf == 3 && ultimaDir != 1) {
								if (Mapa.fanta[x][y + 1] != 9 && Mapa.fanta[x][y + 1] != 7) {
									Mapa.fanta[x][y] = 0;
									y++;
									Mapa.fanta[x][y] = f;
									ultimaDir = Dirf;
								} else if (Mapa.fanta[x][y + 1] == 7) {
									Mapa.fanta[x][y] = 0;
									y = 1;
									Mapa.fanta[x][y] = f;
								}

							} else if (Dirf == 4 && ultimaDir != 2) {
								if (Mapa.fanta[x - 1][y] != 9) {
									Mapa.fanta[x][y] = 0;
									x--;
									Mapa.fanta[x][y] = f;
									ultimaDir = Dirf;
								}

							}
						}
					} else {
						if (Mapa.fanta[x - 1][y] != 9 && Mapa.fanta[x - 1][y] != 8 && ultimaDir != 2) {
							x--;
							ultimaDir = 4;
						} else {
							if (Dirf == 1 && ultimaDir != 3) {
								if (Mapa.fanta[x][y - 1] != 9 && Mapa.fanta[x][y - 1] != 7) {
									Mapa.fanta[x][y] = 0;
									y--;
									Mapa.fanta[x][y] = f;
									ultimaDir = Dirf;
								} else if (Mapa.fanta[x][y - 1] == 7) {
									Mapa.fanta[x][y] = 0;
									y = Mapa.fanta.length - 1;
									Mapa.fanta[x][y] = f;

								}
							} else if (Dirf == 3 && ultimaDir != 1) {
								if (Mapa.fanta[x][y + 1] != 9 && Mapa.fanta[x][y + 1] != 7) {
									Mapa.fanta[x][y] = 0;
									y++;
									Mapa.fanta[x][y] = f;
									ultimaDir = Dirf;
								} else if (Mapa.fanta[x][y + 1] == 7) {
									Mapa.fanta[x][y] = 0;
									y = 1;
									Mapa.fanta[x][y] = f;
								}

							} else if (Dirf == 2 && ultimaDir != 4 && Mapa.fanta[x + 1][y] != 8) {
								if (Mapa.fanta[x + 1][y] != 9) {
									Mapa.fanta[x][y] = 0;
									x++;
									Mapa.fanta[x][y] = f;
									ultimaDir = Dirf;
								}
							}
						}
					}
				} else {
					if (disty > 0) {
						if (Mapa.fanta[x][y + 1] != 9 && Mapa.fanta[x][y + 1] != 8 && ultimaDir != 1) {
							y++;
							ultimaDir = 3;
						} else {
							if (Dirf == 1 && ultimaDir != 3) {
								if (Mapa.fanta[x][y - 1] != 9 && Mapa.fanta[x][y - 1] != 7) {
									Mapa.fanta[x][y] = 0;
									y--;
									Mapa.fanta[x][y] = f;
									ultimaDir = Dirf;
								} else if (Mapa.fanta[x][y - 1] == 7) {
									Mapa.fanta[x][y] = 0;
									y = Mapa.fanta.length - 1;
									Mapa.fanta[x][y] = f;

								}
							} else if (Dirf == 2 && ultimaDir != 4) {
								if (Mapa.fanta[x + 1][y] != 9 && Mapa.fanta[x + 1][y] != 8) {
									Mapa.fanta[x][y] = 0;
									x++;
									Mapa.fanta[x][y] = f;
									ultimaDir = Dirf;
								}

							} else if (Dirf == 4 && ultimaDir != 2) {
								if (Mapa.fanta[x - 1][y] != 9) {
									Mapa.fanta[x][y] = 0;
									x--;
									Mapa.fanta[x][y] = f;
									ultimaDir = Dirf;
								}
							}
						}
					} else {
						if (Mapa.fanta[x][y - 1] != 9 && Mapa.fanta[x][y - 1] != 8 && ultimaDir != 3) {
							y--;
							ultimaDir = 1;
						} else {
							if (Dirf == 2 && ultimaDir != 4) {
								if (Mapa.fanta[x + 1][y] != 9 && Mapa.fanta[x + 1][y] != 8) {
									Mapa.fanta[x][y] = 0;
									x++;
									Mapa.fanta[x][y] = f;
									ultimaDir = Dirf;
								}
							} else if (Dirf == 3 && ultimaDir != 1) {
								if (Mapa.fanta[x][y + 1] != 9 && Mapa.fanta[x][y + 1] != 7) {
									Mapa.fanta[x][y] = 0;
									y++;
									Mapa.fanta[x][y] = f;
									ultimaDir = Dirf;
								} else if (Mapa.fanta[x][y + 1] == 7) {
									Mapa.fanta[x][y] = 0;
									y = 1;
									Mapa.fanta[x][y] = f;
								}

							} else if (Dirf == 4 && ultimaDir != 2) {
								if (Mapa.fanta[x - 1][y] != 9) {
									Mapa.fanta[x][y] = 0;
									x--;
									Mapa.fanta[x][y] = f;
									ultimaDir = Dirf;
								}
							}
						}
					}
				}
				Mapa.fanta[x][y] = f;
			} else if (f == 3 && PACMAN.droja == false) {
				int pacx = 0;
				int pacy = 0;
				for (int i = 0; i < Mapa.campo.length; i++) {
					for (int j = 0; j < Mapa.campo[0].length; j++) {
						if (Mapa.campo[i][j] == 1) {
							pacx = i;
							pacy = j;
						}
					}
				}
				int distx = pacx - x;
				int disty = pacy - y;
				Mapa.fanta[x][y] = 0;
				// (1 arriba, 2 derecha, 3 abajo, 4 izquierda)
				if (Math.abs(distx) > Math.abs(disty)) {
					if (distx > 0) {
						if (Mapa.fanta[x + 1][y] != 9 && Mapa.fanta[x + 1][y] != 8 && ultimaDir != 4) {
							x++;
							ultimaDir = 2;
						} else {
							if (Mapa.fanta[x + 1][y] != 9 && Mapa.fanta[x + 1][y] != 8 && PACMAN.pac.ultimaDir == 'd'
									&& ultimaDir != 4) {
								x++;
								ultimaDir = 2;
							} else if (Mapa.fanta[x - 1][y] != 9 && Mapa.fanta[x - 1][y] != 8
									&& PACMAN.pac.ultimaDir == 'a' && ultimaDir != 2) {
								x--;
								ultimaDir = 4;
							} else if (Mapa.fanta[x][y - 1] != 9 && Mapa.fanta[x][y - 1] != 8
									&& PACMAN.pac.ultimaDir == 'w' && ultimaDir != 3) {
								y--;
								ultimaDir = 1;
							} else if (Mapa.fanta[x][y + 1] != 9 && Mapa.fanta[x][y + 1] != 8
									&& PACMAN.pac.ultimaDir == 's' && ultimaDir != 1) {
								y++;
								ultimaDir = 3;
							} else {
								moverseRandom();
							}
						}
					} else {
						if (Mapa.fanta[x - 1][y] != 9 && Mapa.fanta[x - 1][y] != 8 && ultimaDir != 2) {
							x--;
							ultimaDir = 4;
						} else {
							if (Mapa.fanta[x + 1][y] != 9 && Mapa.fanta[x + 1][y] != 8 && PACMAN.pac.ultimaDir == 'd'
									&& ultimaDir != 4) {
								x++;
								ultimaDir = 2;
							} else if (Mapa.fanta[x - 1][y] != 9 && Mapa.fanta[x - 1][y] != 8
									&& PACMAN.pac.ultimaDir == 'a' && ultimaDir != 2) {
								x--;
								ultimaDir = 4;
							} else if (Mapa.fanta[x][y - 1] != 9 && Mapa.fanta[x][y - 1] != 8
									&& PACMAN.pac.ultimaDir == 'w' && ultimaDir != 3) {
								y--;
								ultimaDir = 1;
							} else if (Mapa.fanta[x][y + 1] != 9 && Mapa.fanta[x][y + 1] != 8
									&& PACMAN.pac.ultimaDir == 's' && ultimaDir != 1) {
								y++;
								ultimaDir = 3;
							} else {
								moverseRandom();
							}
						}
					}
				} else {
					if (disty > 0) {
						if (Mapa.fanta[x][y + 1] != 9 && Mapa.fanta[x][y + 1] != 8 && ultimaDir != 1) {
							y++;
							ultimaDir = 3;
						} else {
							if (Mapa.fanta[x + 1][y] != 9 && Mapa.fanta[x + 1][y] != 8 && PACMAN.pac.ultimaDir == 'd'
									&& ultimaDir != 4) {
								x++;
								ultimaDir = 2;
							} else if (Mapa.fanta[x - 1][y] != 9 && Mapa.fanta[x - 1][y] != 8
									&& PACMAN.pac.ultimaDir == 'a' && ultimaDir != 2) {
								x--;
								ultimaDir = 4;
							} else if (Mapa.fanta[x][y - 1] != 9 && Mapa.fanta[x][y - 1] != 8
									&& PACMAN.pac.ultimaDir == 'w' && ultimaDir != 3) {
								y--;
								ultimaDir = 1;
							} else if (Mapa.fanta[x][y + 1] != 9 && Mapa.fanta[x][y + 1] != 8
									&& PACMAN.pac.ultimaDir == 's' && ultimaDir != 1) {
								y++;
								ultimaDir = 3;
							} else {
								moverseRandom();
							}
						}
					} else {
						if (Mapa.fanta[x][y - 1] != 9 && Mapa.fanta[x][y - 1] != 8 && ultimaDir != 3) {
							y--;
							ultimaDir = 1;
						} else {
							if (Mapa.fanta[x + 1][y] != 9 && Mapa.fanta[x + 1][y] != 8 && PACMAN.pac.ultimaDir == 'd'
									&& ultimaDir != 4) {
								x++;
								ultimaDir = 2;
							} else if (Mapa.fanta[x - 1][y] != 9 && Mapa.fanta[x - 1][y] != 8
									&& PACMAN.pac.ultimaDir == 'a' && ultimaDir != 2) {
								x--;
								ultimaDir = 4;
							} else if (Mapa.fanta[x][y - 1] != 9 && Mapa.fanta[x][y - 1] != 8
									&& PACMAN.pac.ultimaDir == 'w' && ultimaDir != 3) {
								y--;
								ultimaDir = 1;
							} else if (Mapa.fanta[x][y + 1] != 9 && Mapa.fanta[x][y + 1] != 8
									&& PACMAN.pac.ultimaDir == 's' && ultimaDir != 1) {
								y++;
								ultimaDir = 3;
							} else {
								moverseRandom();
							}
						}
					}
				}
				Mapa.fanta[x][y] = f;
			} else {
				if (Dirf == 1 && ultimaDir != 3 && Mapa.fanta[x][y - 1] != 9) {
					ultimaDir = 1;
				} else if (Dirf == 2 && ultimaDir != 4 && Mapa.fanta[x + 1][y] != 9) {
					ultimaDir = 2;
				} else if (Dirf == 3 && ultimaDir != 1 && Mapa.fanta[x][y + 1] != 9) {
					ultimaDir = 3;
				} else if (Dirf == 4 && ultimaDir != 2 && Mapa.fanta[x - 1][y] != 9) {
					ultimaDir = 4;
				}
				if (ultimaDir == 1) {
					if (Mapa.fanta[x][y - 1] != 9 && Mapa.fanta[x][y - 1] != 7) {
						Mapa.fanta[x][y] = 0;
						y--;
						Mapa.fanta[x][y] = f;
					} else if (Mapa.fanta[x][y - 1] == 7) {
						Mapa.fanta[x][y] = 0;
						y = Mapa.fanta.length - 1;
						Mapa.fanta[x][y] = f;

					}
				} else if (ultimaDir == 2) {
					if (Mapa.fanta[x + 1][y] != 9 && Mapa.fanta[x + 1][y] != 8) {
						Mapa.fanta[x][y] = 0;
						x++;
						Mapa.fanta[x][y] = f;
					}

				} else if (ultimaDir == 3) {
					if (Mapa.fanta[x][y + 1] != 9 && Mapa.fanta[x][y + 1] != 7) {
						Mapa.fanta[x][y] = 0;
						y++;
						Mapa.fanta[x][y] = f;
					} else if (Mapa.fanta[x][y + 1] == 7) {
						Mapa.fanta[x][y] = 0;
						y = 1;
						Mapa.fanta[x][y] = f;
					}

				} else if (ultimaDir == 4) {
					if (Mapa.fanta[x - 1][y] != 9) {
						Mapa.fanta[x][y] = 0;
						x--;
						Mapa.fanta[x][y] = f;
					}
				}
			}
			if (Mapa.campo[x][y] == 1) {
				if (PACMAN.droja) {
					Mapa.fanta[x][y] = 0;
					muerto = true;
					cooldown = 5;
					generarFantasma();
					PACMAN.sumarScore(f);
				} else {
					Mapa.campo[x][y] = 0;
					PACMAN.derrota = true;
				}
			}
		} else if (cooldown > 0) {
			cooldown--;
		}
	}

	public void moverseRandom() {
		if (cooldown == 0 && !muerto) {
			if (PACMAN.droja && PACMAN.contadordroja > 14) {
				int pacx = 0;
				int pacy = 0;
				for (int i = 0; i < Mapa.campo.length; i++) {
					for (int j = 0; j < Mapa.campo[0].length; j++) {
						if (Mapa.campo[i][j] == 1) {
							pacx = i;
							pacy = j;
						}
					}
				}
				int distx = pacx - x;
				int disty = pacy - y;
				// (1 arriba, 2 derecha, 3 abajo, 4 izquierda)
				if (Math.abs(distx) > Math.abs(disty)) {
					if (distx > 0) {
						if (Mapa.fanta[x + 1][y] != 9 && Mapa.fanta[x + 1][y] != 8) {
							ultimaDir = 2;
						} else if (Mapa.fanta[x][y - 1] != 9 && Mapa.fanta[x][y - 1] != 8) {
							ultimaDir = 1;
						} else if (Mapa.fanta[x][y + 1] != 9 && Mapa.fanta[x][y + 1] != 8) {
							ultimaDir = 3;
						}
					} else {
						if (Mapa.fanta[x - 1][y] != 9 && Mapa.fanta[x - 1][y] != 8) {
							ultimaDir = 4;
						} else if (Mapa.fanta[x][y - 1] != 9 && Mapa.fanta[x][y - 1] != 8) {
							ultimaDir = 1;
						} else if (Mapa.fanta[x][y + 1] != 9 && Mapa.fanta[x][y + 1] != 8) {
							ultimaDir = 3;
						}
					}
				} else {
					if (disty < 0) {
						if (Mapa.fanta[x][y + 1] != 9 && Mapa.fanta[x][y + 1] != 8) {
							ultimaDir = 3;
						} else if (Mapa.fanta[x - 1][y] != 9 && Mapa.fanta[x - 1][y] != 8) {
							ultimaDir = 4;
						} else if (Mapa.fanta[x + 1][y] != 9 && Mapa.fanta[x + 1][y] != 8) {
							ultimaDir = 2;
						}
					} else {
						if (Mapa.fanta[x][y - 1] != 9 && Mapa.fanta[x][y - 1] != 8) {
							ultimaDir = 1;
						} else if (Mapa.fanta[x - 1][y] != 9 && Mapa.fanta[x - 1][y] != 8) {
							ultimaDir = 4;
						} else if (Mapa.fanta[x + 1][y] != 9 && Mapa.fanta[x + 1][y] != 8) {
							ultimaDir = 2;
						}
					}
				}
			} else {
				Random r = new Random();
				int Dirf = r.nextInt(4) + 1;
				boolean nomuro = false;
				while (!nomuro) {
					if (Dirf == 1 && (Mapa.fanta[x][y - 1] == 9 || ultimaDir == 3)) {
						Dirf = r.nextInt(4) + 1;
					} else {
						nomuro = true;
					}
					if (Dirf == 2 && (Mapa.fanta[x + 1][y] == 9 || ultimaDir == 4)) {
						Dirf = r.nextInt(4) + 1;
					} else {
						nomuro = true;
					}
					if (Dirf == 3 && (Mapa.fanta[x][y + 1] == 9 || ultimaDir == 1)) {
						Dirf = r.nextInt(4) + 1;
					} else {
						nomuro = true;
					}
					if (Dirf == 4 && (Mapa.fanta[x - 1][y] == 9 || ultimaDir == 2)) {
						Dirf = r.nextInt(4) + 1;
					} else {
						nomuro = true;
					}
				}
				if (Dirf == 1 && ultimaDir != 3 && Mapa.fanta[x][y - 1] != 9) {
					ultimaDir = 1;
				} else if (Dirf == 2 && ultimaDir != 4 && Mapa.fanta[x + 1][y] != 9) {
					ultimaDir = 2;
				} else if (Dirf == 3 && ultimaDir != 1 && Mapa.fanta[x][y + 1] != 9) {
					ultimaDir = 3;
				} else if (Dirf == 4 && ultimaDir != 2 && Mapa.fanta[x - 1][y] != 9) {
					ultimaDir = 4;
				}
			}
			if (ultimaDir == 1) {
				if (Mapa.fanta[x][y - 1] != 9 && Mapa.fanta[x][y - 1] != 7) {
					Mapa.fanta[x][y] = 0;
					y--;
					Mapa.fanta[x][y] = f;
				} else if (Mapa.fanta[x][y - 1] == 7) {
					Mapa.fanta[x][y] = 0;
					y = Mapa.fanta.length - 1;
					Mapa.fanta[x][y] = f;

				}
			} else if (ultimaDir == 2) {
				if (Mapa.fanta[x + 1][y] != 9 && Mapa.fanta[x + 1][y] != 8) {
					Mapa.fanta[x][y] = 0;
					x++;
					Mapa.fanta[x][y] = f;
				}

			} else if (ultimaDir == 3) {
				if (Mapa.fanta[x][y + 1] != 9 && Mapa.fanta[x][y + 1] != 7) {
					Mapa.fanta[x][y] = 0;
					y++;
					Mapa.fanta[x][y] = f;
				} else if (Mapa.fanta[x][y + 1] == 7) {
					Mapa.fanta[x][y] = 0;
					y = 1;
					Mapa.fanta[x][y] = f;
				}

			} else if (ultimaDir == 4) {
				if (Mapa.fanta[x - 1][y] != 9) {
					Mapa.fanta[x][y] = 0;
					x--;
					Mapa.fanta[x][y] = f;
				}
			}
		}

		if (Mapa.campo[x][y] == 1)

		{
			if (PACMAN.droja) {
				Mapa.fanta[x][y] = 0;
				muerto = true;
				cooldown = 5;
				generarFantasma();
				PACMAN.sumarScore(f);
			} else {
				Mapa.campo[x][y] = 0;
				PACMAN.derrota = true;
			}

		} else if (cooldown > 0) {
			cooldown--;
		}
	}

//	TimerTask timertask = new TimerTask() {
//		@Override
//		public void run() {
//			moverseRandom();
//		}
//	};
//	Timer timer = new Timer();
//
//	void ponertimer() {
//		timer.scheduleAtFixedRate(timertask, 0, 750);
//	}
	/**
	 * Función que pone un nuevo fantasma en la matriz, si está muerto y no está en
	 * cooldown
	 */
	public void generarFantasma() {
		if (muerto && cooldown == 0) {
			muerto = false;
			x = 8;
			y = 11;
			Mapa.fanta[x][y] = f;
		}
	}
}

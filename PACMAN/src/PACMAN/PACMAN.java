package PACMAN;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;
import PACMAN.Window;

/**
 * El propio juego
 * 
 * @author Ethan Escribá
 * @version 3.0
 *
 */
public class PACMAN {
	static Scanner sc = new Scanner(System.in);
	static Board f = new Board();
	static Window w = new Window(f);
	/**
	 * Matriz vacía
	 */
	static int[][] nada = { { 0, 0 }, { 0, 0 } };
	/**
	 * Booleano de si pierdes
	 */
	static boolean derrota;
	/**
	 * Boolean de el powerup
	 */
	static boolean droja;
	/**
	 * Contador del powerup
	 */
	static int contadordroja = 15;
	/**
	 * Lo que se va a imprimir por pantalla.
	 */
	static int[][][] pantalla;
	/**
	 * Puntuación.
	 */
	static int Score;
	/**
	 * Es el ranking
	 */
	static ArrayList<Ranking> Rankings = new ArrayList<>();
	/**
	 * Si quedan cosas que comerte
	 */
	static boolean cala;
	/**
	 * Lista que contiene todos los fantasmas
	 */
	static ArrayList<Fantasma> fantasmas = new ArrayList<>();
	/**
	 * Tu personaje
	 */
	static PacPersonaje pac;
	/**
	 * El mapa
	 */
	static Mapa mopa;
	/**
	 * Nivel en el que estamos
	 */
	static int nivel = 1;

	public static void main(String[] args) throws InterruptedException, IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		// Moverse: wasd
		File f = new File("rankings/Ranking.txt");
		if (f.exists()) {
			cargarRanking();
		}
		boolean salir = false;
		while (!salir) {
			menu();
			int num = ponerOpcion();
			switch (num) {
			case 1:
				// Jugar (Por defecto nivel 1)
				Jugar();
				break;
			case 2:
				// Niveles
				selectarNivel();
				System.out.println("Nivel " + nivel + " seleccionado.");
				break;
			case 3:
				// Ranking
				mostrarRanking();
				break;
			}
		}
	}

	private static void cargarRanking() throws ClassNotFoundException, IOException {
		// TODO Auto-generated method stub
		File f = new File("rankings/Ranking.txt");
		if (f.length() > 0 && f.exists()) {
			FileInputStream fis = new FileInputStream(f);
			ObjectInputStream ois = new ObjectInputStream(fis);
			Object o = ois.readObject();
			Rankings = (ArrayList<Ranking>) o;
			ois.close();
		}
	}

	/**
	 * Seleccionas el nivel que quieres
	 */
	private static void selectarNivel() {
		// TODO Auto-generated method stub
		System.out.println("Nivel 0: Memelevel");
		System.out.println("Nivel 1: Classic");
		int level = Integer.parseInt(w.showInputPopup("Level(0 o 1)"));
		nivel = level;
	}

	/**
	 * Menú del juego
	 */
	private static void menu() {
		int[][] textoaqui = { { 1 }, { 2 }, { 3 } };
		f.setActimgbackground(true);
		f.setImgbackground("titlescree.jpg");
		f.setColorbackground(0x000000);
		f.setActborder(false);
		String[] lletres = { "", "INICIAR", "NIVELES", "RANKING" };
		f.setText(lletres);
		int[] colorlletres = { 0xFFFFFF, 0xFFFFFF, 0xFFFFFF, 0xFFFFFF };
		f.setColortext(colorlletres);
		f.draw(textoaqui, 't');
	}

	/**
	 * Donde se desarrolla el propio juego
	 * 
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private static void Jugar() throws InterruptedException, IOException, ClassNotFoundException {
		Inicia();
		boolean partida = true;
		int contadorparafruta = 0;
		char direccion = 's';
		w.playMusic("pacman_chomp.wav");
		Thread.sleep(200);
		f.setActimgbackground(false);
		while (partida) {
			if (contadorparafruta == 100) {
				Mapa.ponerFruta();
				contadorparafruta = 0;
			}
			contadorparafruta++;
			direccion = w.getKeyPressed();
			if (direccion == 'g') {
				saveState();
			} else if (direccion == 'c') {
				loadState();
			} else {
				pac.moverse(direccion);
			}
			morirse();
			for (Fantasma fan : fantasmas) {
				fan.generarFantasma();
				if (droja) {
					if (fan.movfan > 1) {
						fan.movfan = 0;
						fan.moverseRandom();
					}
					fan.movfan++;
				} else {
					fan.moverse();
				}
			}
			if (droja) {
				contadordroja--;
				if (contadordroja == 0) {
					droja = false;
					w.stopMusic();
					w.playMusic("pacman_chomp.wav");
				}
			} else {
				f.setActimgbackground(false);
			}
			String[] etics = { "Nivel: " + nivel, "Score: " + Score };
			w.setLabels(etics);
			f.draw(pantalla);
			comprobarCalabazas();
			partida = comprobarAcabar();
			Thread.sleep(360);
		}
		w.stopMusic();
		Vicotory();
		Thread.sleep(2000);

	}

	private static void loadState() throws IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		File fi = new File("save/Save.txt");
		if (fi.length() > 0 && fi.exists()) {
			FileInputStream fis = new FileInputStream(fi);
			ObjectInputStream ois = new ObjectInputStream(fis);
			derrota = ois.readBoolean();
			droja = ois.readBoolean();
			contadordroja = ois.readInt();
			Mapa.campo = (int[][]) ois.readObject();
			Mapa.bolas = (int[][]) ois.readObject();
			Mapa.fanta = (int[][]) ois.readObject();
			int[][][] cosa = { Mapa.campo, Mapa.bolas, Mapa.fanta };
			pantalla = cosa;
			Score = ois.readInt();
			cala = ois.readBoolean();
			pac = (PacPersonaje) ois.readObject();
			nivel = ois.readInt();
			fantasmas.clear();
			fantasmas = (ArrayList<Fantasma>) ois.readObject();
			f.draw(pantalla);
			ois.close();
			if (nivel == 0) {
				String[] imatges = { "", "pacman.gif", "ping.jpg", "salad.png", "lata.png", "al.png", "power.png",
						"portal.gif", "", "negro.jpg", "manz.png" };
				f.setSprites(imatges);
			} else {
				String[] imatges = { "", "PACMA.gif", "fantas.png", "fantas2.png", "fantas3.png", "bolama.png",
						"cama.png", "", "", "negro.jpg", "manz.png" };
				f.setSprites(imatges);
			}
		}
	}

	private static void saveState() throws IOException {
		File f = new File("save/Save.txt");
		if (f.exists()) {
			f.delete();
		}
		f.createNewFile();
		FileOutputStream fos = new FileOutputStream(f);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeBoolean(derrota);
		oos.writeBoolean(droja);
		oos.writeInt(contadordroja);
		oos.writeObject(Mapa.campo);
		oos.writeObject(Mapa.bolas);
		oos.writeObject(Mapa.fanta);
		oos.writeInt(Score);
		oos.writeBoolean(cala);
		oos.writeObject(pac);
		oos.writeInt(nivel);
		oos.writeObject(fantasmas);
		oos.flush();
		oos.close();
	}

	/**
	 * Ver si tu personaje ha pisado un fantasma y muere conseqüentemente.
	 */
	private static void morirse() {
		// TODO Auto-generated method stub
		if (Mapa.fanta[pac.x][pac.y] == 2) {
			if (droja) {
				Mapa.fanta[pac.x][pac.y] = 0;
				fantasmas.get(0).muerto = true;
				fantasmas.get(0).cooldown = 5;
				fantasmas.get(0).generarFantasma();
				sumarScore(2);
			} else {
				Mapa.campo[pac.x][pac.y] = 0;
				pac.x = 0;
				derrota = true;
			}
		}
		if (Mapa.fanta[pac.x][pac.y] == 3) {
			if (droja) {
				Mapa.fanta[pac.x][pac.y] = 0;
				fantasmas.get(1).muerto = true;
				fantasmas.get(1).cooldown = 5;
				fantasmas.get(1).generarFantasma();
				sumarScore(3);
			} else {
				Mapa.campo[pac.x][pac.y] = 0;
				pac.x = 0;
				derrota = true;
			}
		}
		if (Mapa.fanta[pac.x][pac.y] == 4) {
			if (droja) {
				Mapa.fanta[pac.x][pac.y] = 0;
				fantasmas.get(2).muerto = true;
				fantasmas.get(2).cooldown = 5;
				fantasmas.get(2).generarFantasma();
				sumarScore(4);
			} else {
				Mapa.campo[pac.x][pac.y] = 0;
				pac.x = 0;
				derrota = true;
			}
		}

	}

	/**
	 * Para seleccionar cosas en el menú
	 * 
	 * @return num (la cosa seleccionada)
	 */
	private static int ponerOpcion() {
		int fi = f.getCurrentMouseRow();
		int c = f.getCurrentMouseCol();
		do {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
			}
			fi = f.getCurrentMouseRow();
			c = f.getCurrentMouseCol();
		} while (fi == -1 && c == -1);
		int num = 0;
		if (fi != -1 && c != -1) {
			switch (fi) {
			case 0:
				num = 1;
				break;
			case 1:
				num = 2;
				break;
			case 2:
				num = 3;
				break;
			}
		}
		return num;
	}

	/**
	 * Te muestra el ranking de jugadores y su puntuación
	 */
	private static void mostrarRanking() {
		// TODO Auto-generated method stub
		for (Ranking ranking : Rankings) {
			System.out.println(ranking);
		}
		System.out.println("---------------------------------------------------------------------");
	}

	/**
	 * Ver si has ganado o muerto miserablemente
	 * 
	 * @throws IOException
	 */
	private static void Vicotory() throws IOException {
		// TODO Auto-generated method stub
		Random r = new Random();
		if (cala == false) {
			System.out.println("Has ganado");
			f.draw(nada);
			f.setActimgbackground(true);
			f.setImgbackground("win.gif");
		} else {
			f.draw(nada);
			f.setActimgbackground(true);
			f.setImgbackground("muerte.jpg");
		}
		String nombre = "";
		while (nombre.equals("")) {
			System.out.println("Pon tu nombre para el ranking:");
			nombre = w.showInputPopup("Pon tu nombre para el ranking:");
			if (nombre.equals("")) {
				System.out.println("Pon un nombre no vacío.");
			}
		}
		if (cala == false) {
			Rankings.add(new Ranking(nombre, Score, nivel, true));
		} else if (nombre.equals("Nathaniel") || nombre.equals("La Diosa") || nombre.equals("Sacapuntas")
				|| nombre.equals("Adolfito")) {
			Rankings.add(new Ranking(nombre, (r.nextInt(350000) + 35), nivel, true));
		} else {
			Rankings.add(new Ranking(nombre, Score, nivel, false));
		}
		guardarRanking();
	}

	private static void guardarRanking() throws IOException {
		// TODO Auto-generated method stub
		Collections.sort(Rankings);
		File f = new File("rankings/Ranking.txt");
		f.createNewFile();
		FileOutputStream fos = new FileOutputStream(f);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(Rankings);
		oos.flush();
		oos.close();

		File f2 = new File("rankings/RankingLeer.txt");
		FileWriter out = new FileWriter(f2);
		BufferedWriter bw = new BufferedWriter(out);
		for (Ranking ranking : Rankings) {
			bw.write(ranking.toString());
			bw.newLine();
		}
		bw.flush();
		bw.close();
	}

	/**
	 * Miras si quedan cosas para comer
	 */
	private static void comprobarCalabazas() {
		// TODO Auto-generated method stub
		cala = false;
		for (int i = 0; i < Mapa.bolas.length; i++) {
			for (int j = 0; j < Mapa.bolas[0].length; j++) {
				if (Mapa.bolas[i][j] == 5 || Mapa.bolas[i][j] == 6) {
					cala = true;
				}
			}
		}
	}

	/**
	 * Sumas score dependiendo de que te comes
	 * 
	 * @param i si es 5, es comida normal, si es 6 o 10, fruta grande i si es 2,3,4,
	 *          es un fantasma.
	 */
	public static void sumarScore(int i) {
		// TODO Auto-generated method stub
		if (i == 5) {
			Score = Score + 50;
		} else if (i == 6 || i == 10) {
			Score = Score + 200;
		}
		if (i == 2 || i == 3 || i == 4) {
			Score = Score + 300;
		}
	}

	/**
	 * Al comer el powerup te permite comer fantasmas
	 */
	public static void powerup() {
		// TODO Auto-generated method stub
		f.setActimgbackground(true);
		droja = true;
		f.setImgbackground("droja.png");
		contadordroja = 18;
		w.playMusic("dejavu.wav");
	}

	/**
	 * Mira donde estas
	 */
	public static void verDondeEstas() {
		// TODO Auto-generated method stub
		for (int i = 0; i < Mapa.campo.length; i++) {
			for (int j = 0; j < Mapa.campo[0].length; j++) {
				if (Mapa.campo[i][j] == 1) {
					pac.x = i;
					pac.y = j;
				}
			}
		}
	}

	/**
	 * Mira si la partida continua
	 * 
	 * @return true si continua, false si acaba
	 */
	private static boolean comprobarAcabar() {
		// TODO Auto-generated method stub
		if (cala == false || derrota == true) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Inicializador del campo y demás
	 */
	private static void Inicia() {
		// TODO Auto-generated method stub
		Initcampo();
		pac = new PacPersonaje(16, 11);
		fantasmas = new ArrayList<>();
		Fantasma f1 = new Fantasma(8, 11, 2);
		Fantasma f2 = new Fantasma(8, 11, 3);
		Fantasma f3 = new Fantasma(8, 11, 4);
		fantasmas.add(f1);
		fantasmas.add(f2);
		fantasmas.add(f3);
		w.setActLabels(true);
		f.setActborder(false);
		f.setColorbackground(0xffffff);
		w.setTitle("PAC-MAN");
		if (nivel == 0) {
			String[] imatges = { "", "pacman.gif", "ping.jpg", "salad.png", "lata.png", "al.png", "power.png",
					"portal.gif", "", "negro.jpg", "manz.png" };
			f.setSprites(imatges);
		} else {
			String[] imatges = { "", "PACMA.gif", "fantas.png", "fantas2.png", "fantas3.png", "bolama.png", "cama.png",
					"", "", "negro.jpg", "manz.png" };
			f.setSprites(imatges);
		}
		droja = false;
		derrota = false;
		cala = true;
		Score = 0;
		ponerPacman();
		for (Fantasma fan : fantasmas) {
			fan.cooldown = 0;
		}
		pac.ultimaDir = 's';
	}

	/**
	 * Pone al pacman en la matriz
	 */
	private static void ponerPacman() {
		// TODO Auto-generated method stub
		Mapa.campo[16][11] = 1;
	}

	/**
	 * Te inicia pantalla con el campo, bolas y fantasmas
	 */
	private static void Initcampo() {
		// TODO Auto-generated method stub
		Mapa.generarmapa1();
		int[][][] cosa = { Mapa.campo, Mapa.bolas, Mapa.fanta };
		pantalla = cosa;

	}

}

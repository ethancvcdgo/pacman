package PACMAN;

import java.io.Serializable;
import java.util.Date;

public class Ranking implements Comparable, Serializable {
	String nombre;
	int puntos;
	int nivel;
	boolean ganado;
	Date fecha;

	public Ranking(String nombre, int puntos, int nivel, boolean ganado) {
		this.nombre = nombre;
		this.puntos = puntos;
		this.nivel = nivel;
		this.ganado = ganado;
		this.fecha = new Date();
	}

	@Override
	public int compareTo(Object arg0) {
		// TODO Auto-generated method stub
		Ranking r = (Ranking) arg0;
		if (r.puntos < this.puntos) {
			return -1;
		} else if (r.puntos == this.puntos) {
			return 0;
		} else {
			return 1;
		}
	}

	@Override
	public String toString() {
		return "Ranking [Nombre=" + nombre + ", Puntuación=" + puntos + ", Nivel=" + nivel + ", Victoria=" + ganado
				+ ", Fecha=" + fecha + "]";
	}
}

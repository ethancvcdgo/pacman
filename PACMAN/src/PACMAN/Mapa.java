package PACMAN;

import java.util.Random;

public class Mapa {
	/**
	 * Matriz base con los muros y pacman.
	 */
	static int[][] campo;

	/**
	 * Matriz con las bolas normales y especiales.
	 */
	static int[][] bolas;

	/**
	 * Matriz con fantasmas y movimientos
	 */
	static int[][] fanta;

	/**
	 * Te genera el mapa por defecto
	 */
	public static void generarmapa1() {
		int[][] campo = { { 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
				{ 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9 },
				{ 9, 0, 9, 9, 9, 0, 9, 9, 9, 9, 0, 9, 0, 9, 9, 9, 9, 0, 9, 9, 9, 0, 9 },
				{ 9, 0, 9, 9, 9, 0, 9, 9, 9, 9, 0, 9, 0, 9, 9, 9, 9, 0, 9, 9, 9, 0, 9 },
				{ 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9 },
				{ 9, 0, 9, 9, 9, 0, 9, 0, 9, 9, 9, 9, 9, 9, 9, 0, 9, 0, 9, 9, 9, 0, 9 },
				{ 9, 0, 0, 0, 0, 0, 9, 0, 0, 0, 0, 9, 0, 0, 0, 0, 9, 0, 0, 0, 0, 0, 9 },
				{ 9, 9, 9, 9, 9, 0, 9, 9, 9, 9, 0, 9, 0, 9, 9, 9, 9, 0, 9, 9, 9, 9, 9 },
				{ 9, 9, 9, 9, 9, 0, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 0, 9, 9, 9, 9, 9 },
				{ 9, 9, 9, 9, 9, 0, 9, 0, 9, 9, 9, 8, 9, 9, 9, 0, 9, 0, 9, 9, 9, 9, 9 },
				{ 7, 0, 0, 0, 0, 0, 0, 0, 9, 0, 0, 0, 0, 0, 9, 0, 0, 0, 0, 0, 0, 0, 7 },
				{ 9, 9, 9, 9, 9, 0, 9, 0, 9, 9, 9, 9, 9, 9, 9, 0, 9, 0, 9, 9, 9, 9, 9 },
				{ 9, 9, 9, 9, 9, 0, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 0, 9, 9, 9, 9, 9 },
				{ 9, 9, 9, 9, 9, 0, 9, 0, 9, 9, 9, 9, 9, 9, 9, 0, 9, 0, 9, 9, 9, 9, 9 },
				{ 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9 },
				{ 9, 0, 9, 9, 9, 0, 9, 9, 9, 9, 0, 9, 0, 9, 9, 9, 9, 0, 9, 9, 9, 0, 9 },
				{ 9, 0, 0, 0, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 0, 0, 0, 9 },
				{ 9, 9, 9, 0, 9, 0, 9, 0, 9, 9, 9, 9, 9, 9, 9, 0, 9, 0, 9, 0, 9, 9, 9 },
				{ 9, 0, 0, 0, 0, 0, 9, 0, 0, 0, 0, 9, 0, 0, 0, 0, 9, 0, 0, 0, 0, 0, 9 },
				{ 9, 0, 9, 9, 9, 9, 9, 9, 9, 9, 0, 9, 0, 9, 9, 9, 9, 9, 9, 9, 9, 0, 9 },
				{ 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9 },
				{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 } };
		int[][] bolas = { { 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
				{ 9, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 9, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 9 },
				{ 9, 6, 9, 9, 9, 5, 9, 9, 9, 9, 5, 9, 5, 9, 9, 9, 9, 5, 9, 9, 9, 6, 9 },
				{ 9, 5, 9, 9, 9, 5, 9, 9, 9, 9, 5, 9, 5, 9, 9, 9, 9, 5, 9, 9, 9, 5, 9 },
				{ 9, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 9 },
				{ 9, 5, 9, 9, 9, 5, 9, 5, 9, 9, 9, 9, 9, 9, 9, 5, 9, 5, 9, 9, 9, 5, 9 },
				{ 9, 5, 5, 5, 5, 5, 9, 5, 5, 5, 5, 9, 5, 5, 5, 5, 9, 5, 5, 5, 5, 5, 9 },
				{ 9, 9, 9, 9, 9, 5, 9, 9, 9, 9, 0, 9, 0, 9, 9, 9, 9, 5, 9, 9, 9, 9, 9 },
				{ 9, 9, 9, 9, 9, 5, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 5, 9, 9, 9, 9, 9 },
				{ 9, 9, 9, 9, 9, 5, 9, 0, 9, 9, 9, 8, 9, 9, 9, 0, 9, 5, 9, 9, 9, 9, 9 },
				{ 7, 0, 0, 0, 0, 5, 0, 0, 9, 0, 0, 0, 0, 0, 9, 0, 0, 5, 0, 0, 0, 0, 7 },
				{ 9, 9, 9, 9, 9, 5, 9, 0, 9, 9, 9, 9, 9, 9, 9, 0, 9, 5, 9, 9, 9, 9, 9 },
				{ 9, 9, 9, 9, 9, 5, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 5, 9, 9, 9, 9, 9 },
				{ 9, 9, 9, 9, 9, 5, 9, 0, 9, 9, 9, 9, 9, 9, 9, 0, 9, 5, 9, 9, 9, 9, 9 },
				{ 9, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 9, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 9 },
				{ 9, 5, 9, 9, 9, 5, 9, 9, 9, 9, 5, 9, 5, 9, 9, 9, 9, 5, 9, 9, 9, 5, 9 },
				{ 9, 6, 5, 5, 9, 5, 5, 5, 5, 5, 5, 0, 5, 5, 5, 5, 5, 5, 9, 5, 5, 6, 9 },
				{ 9, 9, 9, 5, 9, 5, 9, 5, 9, 9, 9, 9, 9, 9, 9, 5, 9, 5, 9, 5, 9, 9, 9 },
				{ 9, 5, 5, 5, 5, 5, 9, 5, 5, 5, 5, 9, 5, 5, 5, 5, 9, 5, 5, 5, 5, 5, 9 },
				{ 9, 5, 9, 9, 9, 9, 9, 9, 9, 9, 5, 9, 5, 9, 9, 9, 9, 9, 9, 9, 9, 5, 9 },
				{ 9, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 9 },
				{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 } };
		int[][] fanta = { { 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
				{ 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9 },
				{ 9, 0, 9, 9, 9, 0, 9, 9, 9, 9, 0, 9, 0, 9, 9, 9, 9, 0, 9, 9, 9, 0, 9 },
				{ 9, 0, 9, 9, 9, 0, 9, 9, 9, 9, 0, 9, 0, 9, 9, 9, 9, 0, 9, 9, 9, 0, 9 },
				{ 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9 },
				{ 9, 0, 9, 9, 9, 0, 9, 0, 9, 9, 9, 9, 9, 9, 9, 0, 9, 0, 9, 9, 9, 0, 9 },
				{ 9, 0, 0, 0, 0, 0, 9, 0, 0, 0, 0, 9, 0, 0, 0, 0, 9, 0, 0, 0, 0, 0, 9 },
				{ 9, 9, 9, 9, 9, 0, 9, 9, 9, 9, 0, 9, 0, 9, 9, 9, 9, 0, 9, 9, 9, 9, 9 },
				{ 9, 9, 9, 9, 9, 0, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 0, 9, 9, 9, 9, 9 },
				{ 9, 9, 9, 9, 9, 0, 9, 0, 9, 9, 9, 8, 9, 9, 9, 0, 9, 0, 9, 9, 9, 9, 9 },
				{ 7, 0, 0, 0, 0, 0, 0, 0, 9, 0, 3, 2, 4, 0, 9, 0, 0, 0, 0, 0, 0, 0, 7 },
				{ 9, 9, 9, 9, 9, 0, 9, 0, 9, 9, 9, 9, 9, 9, 9, 0, 9, 0, 9, 9, 9, 9, 9 },
				{ 9, 9, 9, 9, 9, 0, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 0, 9, 9, 9, 9, 9 },
				{ 9, 9, 9, 9, 9, 0, 9, 0, 9, 9, 9, 9, 9, 9, 9, 0, 9, 0, 9, 9, 9, 9, 9 },
				{ 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9 },
				{ 9, 0, 9, 9, 9, 0, 9, 9, 9, 9, 0, 9, 0, 9, 9, 9, 9, 0, 9, 9, 9, 0, 9 },
				{ 9, 0, 0, 0, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 0, 0, 0, 9 },
				{ 9, 9, 9, 0, 9, 0, 9, 0, 9, 9, 9, 9, 9, 9, 9, 0, 9, 0, 9, 0, 9, 9, 9 },
				{ 9, 0, 0, 0, 0, 0, 9, 0, 0, 0, 0, 9, 0, 0, 0, 0, 9, 0, 0, 0, 0, 0, 9 },
				{ 9, 0, 9, 9, 9, 9, 9, 9, 9, 9, 0, 9, 0, 9, 9, 9, 9, 9, 9, 9, 9, 0, 9 },
				{ 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9 },
				{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 } };
		Mapa.campo = campo;
		Mapa.bolas = bolas;
		Mapa.fanta = fanta;

	}

	/**
	 * Pone una fruta random en la matriz de bolas
	 */
	static void ponerFruta() {
		// TODO Auto-generated method stub
		Random r = new Random();
		int f = r.nextInt(bolas.length);
		int c = r.nextInt(bolas[0].length);
		if (bolas[f][c] == 0) {
			bolas[f][c] = 10;
		} else {
			ponerFruta();
		}
	}
}

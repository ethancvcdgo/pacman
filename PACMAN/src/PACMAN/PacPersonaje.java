package PACMAN;

import java.io.Serializable;

public class PacPersonaje extends Caracters implements Serializable {
	char ultimaDir;

	public PacPersonaje(int x, int y) {
		super(x, y);
		this.ultimaDir = ' ';
	}

	/**
	 * Como mueves tu pacman
	 * 
	 * @param direccion (dirección que has pulsado para moverte)
	 */
	public void moverse(char direccion) {
		// TODO Auto-generated method stub
		PACMAN.verDondeEstas();
		if (direccion == 'a' && ultimaDir != 'd' && Mapa.campo[x][y - 1] != 9) {
			ultimaDir = 'a';
		} else if (direccion == 's' && ultimaDir != 'w' && Mapa.campo[x + 1][y] != 9 && Mapa.campo[x + 1][y] != 8) {
			ultimaDir = 's';
		} else if (direccion == 'd' && ultimaDir != 'a' && Mapa.campo[x][y + 1] != 9) {
			ultimaDir = 'd';
		} else if (direccion == 'w' && ultimaDir != 's' && Mapa.campo[x - 1][y] != 9) {
			ultimaDir = 'w';
		}
		if (ultimaDir == 'a') {
			if (Mapa.campo[x][y - 1] != 9 && Mapa.campo[x][y - 1] != 7) {
				Mapa.campo[x][y] = 0;
				y--;
				Mapa.campo[x][y] = 1;
			} else if (Mapa.campo[x][y - 1] == 7) {
				Mapa.campo[x][y] = 0;
				y = Mapa.campo.length - 1;
				Mapa.campo[x][y] = 1;
			}

		} else if (ultimaDir == 's') {
			if (Mapa.campo[x + 1][y] != 9 && Mapa.campo[x + 1][y] != 8) {
				Mapa.campo[x][y] = 0;
				x++;
				Mapa.campo[x][y] = 1;
			}

		} else if (ultimaDir == 'd') {
			if (Mapa.campo[x][y + 1] != 9 && Mapa.campo[x][y + 1] != 7) {
				Mapa.campo[x][y] = 0;
				y++;
				Mapa.campo[x][y] = 1;
			} else if (Mapa.campo[x][y + 1] == 7) {
				Mapa.campo[x][y] = 0;
				y = 1;
				Mapa.campo[x][y] = 1;
			}

		} else if (ultimaDir == 'w') {
			if (Mapa.campo[x - 1][y] != 9) {
				Mapa.campo[x][y] = 0;
				x--;
				Mapa.campo[x][y] = 1;
			}

		}
		if (Mapa.bolas[x][y] == 5) {
			Mapa.bolas[x][y] = 0;
			PACMAN.sumarScore(5);
		}
		if (Mapa.bolas[x][y] == 10) {
			Mapa.bolas[x][y] = 0;
			PACMAN.sumarScore(10);
		}
		if (Mapa.bolas[x][y] == 6) {
			Mapa.bolas[x][y] = 0;
			PACMAN.sumarScore(6);
			PACMAN.powerup();
		}
	}

	@Override
	public void moverse() {
	}
}
